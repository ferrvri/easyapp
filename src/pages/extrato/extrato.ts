import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';

/**
 * Generated class for the ExtratoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-extrato',
  templateUrl: 'extrato.html',
})
export class ExtratoPage implements OnInit{

  isLoading = true;
  extract = []
  meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
  
  constructor( private _alert: AlertController, private _http: Http, private viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExtratoPage');
  }

  ngOnInit(): void {
    this.isLoading = true
    let idFilial = (this.navParams.get('idFilial') === null  || this.navParams.get('idFilial') == null ? 0 : this.navParams.get('idFilial'))
    this._http.get(
      'https://apptst.easychopp.com.br/api/getTransacoesCliente?key='
      +JSON.parse(localStorage.getItem('session')).hashKey
      +'&idEmpresa='+this.navParams.get('idCliente')
      +'&idCliente='+JSON.parse(localStorage.getItem('session')).idCliente
      +'&idFilial='+idFilial
      +'&dtInicial='+ (new Date(new Date().getFullYear(), new Date().getMonth(), 1)).toISOString().split('T')[0]
      +'&dtFinal='+(new Date(new Date().getFullYear(), new Date().getMonth()+1, 0)).toISOString().split('T')[0]
      +'&HoursLocalTimeOffSet=-03:00',
      {}
    ).subscribe( (response) => {
      if (!response.json().dsError){
        if (response.json().length > 0){
          console.log('response', response.json());
          let o = {
            dia: 'Mês de ' + this.meses[new Date(Date.now()).getMonth()],
            content: []
          };

          response.json().forEach(element => {
            let data =  new Date(parseInt(element.dtTransacao.replace('/Date(', '').replace(')/', ''))).toISOString().split('T')[0];
            element.dtTransacao = data.split('-')[2] + '/' + data.split('-')[1] + '/'+data.split('-')[0];
            
            o.content.push({
              id: element.idTransacao,
              data: element.dsProduto || element.dsTipoTransacao,
              type: element.dsSiglaMovimento,
              amount: element.vlValorTransacao,
              date: element.dtTransacao
            })
          });
          this.extract.push(o);
          console.log(this.extract);
          this.isLoading = false
        }else{
          this.isLoading = false
        }
      }else{
        this.isLoading = false
      }
    })
  }

  filter(){
    let mes_index = 0;
    let alert = this._alert.create();
    alert.setTitle('Escolha o Mês');

    this.meses.forEach( (element, index) => {
      alert.addInput({
        type: 'radio',
        label: element,
        value: ''+(index),
        checked: false
      });
    });

    alert.addButton('Cancelar');
    alert.addButton({
      text: 'Filtrar',
      handler: data => {
        mes_index = data
        alert2.present()
      }
    });
    alert.present();

    let alert2 = this._alert.create();
    alert2.setTitle('Escolha o Ano');

    for (let i = 0; i <= 50; i++){
      alert2.addInput({
        type: 'radio',
        label: ''+(new Date().getFullYear() + i),
        value: ''+(new Date().getFullYear() + i),
        checked: false
      });
    }

    alert2.addButton('Cancelar');
    alert2.addButton({
      text: 'Filtrar',
      handler: data => {
        this.isLoading = true;
        let idFilial = (this.navParams.get('idFilial') === null  || this.navParams.get('idFilial') == null ? 0 : this.navParams.get('idFilial'))
        this._http.get(
          'https://apptst.easychopp.com.br/api/getTransacoesCliente?key='
          +JSON.parse(localStorage.getItem('session')).hashKey
          +'&idEmpresa='+this.navParams.get('idCliente')
          +'&idCliente='+JSON.parse(localStorage.getItem('session')).idCliente
          +'&idFilial='+idFilial
          +'&dtInicial='+ (new Date(data, mes_index, 1)).toISOString().split('T')[0]
          +'&dtFinal='+(new Date(data, mes_index+1, 1)).toISOString().split('T')[0]
          +'&HoursLocalTimeOffSet=-03:00',
          {}
        ).subscribe( (response) => {
          if (!response.json().dsError){
            console.log('response', response.json());
            let o = {
              dia: 'Mês de ' + this.meses[mes_index],
              content: []
            };
    
            response.json().forEach(element => {
              let data =  new Date(parseInt(element.dtTransacao.replace('/Date(', '').replace(')/', ''))).toISOString().split('T')[0];
              element.dtTransacao = data.split('-')[2] + '/' + data.split('-')[1] + '/'+data.split('-')[0];
              
              o.content.push({
                id: element.idTransacao,
                data: element.dsProduto || element.dsTipoTransacao,
                type: element.dsSiglaMovimento,
                amount: element.vlValorTransacao,
                date: element.dtTransacao
              })
            });
            
            this.extract = []
            this.extract.push(o);
            this.isLoading = false
          }else{
            this.isLoading = false
          }
        })
      }
    });
  }
}