import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController } from 'ionic-angular';
import { Http } from '@angular/http';

import {Md5} from "md5-typescript";

/**
 * Generated class for the CadastraPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cadastra',
  templateUrl: 'cadastra.html',
})
export class CadastraPage {

  nome = '';
  email = '';
  data = '';
  sexo = {
    masculino: false,
    feminino: false
  }
  cpf = '';
  telefone = '';
  senha = '';


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _alert: AlertController,
    private _http: Http,
    public _viewControl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CadastraPage');
  }
  
  setSexo(s){
    switch(s){
      case 'f':{
        this.sexo.masculino = false
        break
      }

      case 'm':{
        this.sexo.feminino = false
        break
      }
    }
  }

  cadastrar(){
    console.log(Md5.init('Teste'));
    if (this.nome.length < 1 || this.email.length < 1 || this.cpf.length < 1 || this.data.length < 1 || this.telefone.length < 1 ){
      let a = this._alert.create({
        title: 'Campos vazios!',
        subTitle: 'Preencha todos os campos para completar o cadastro.',
        buttons: ['OK']
      })
      a.present()
    }else{
      this._http.post(
        'https://apptst.easychopp.com.br/api/addCliente?'+
        'key='+Md5.init('EAS1234CHO5678'+this.cpf)
        +'&dsNomeCliente='+this.nome
        +'&dsEmail='+this.email
        +'&dsSenha='+this.senha
        +'&dtNascimento='+this.data.split('-')[2] + '/' + this.data.split('-')[1] + '/' + this.data.split('-')[0]
        +'&dsSexo='+(this.sexo.feminino == true? 'F': this.sexo.masculino == true ? 'M':'-')
        +'&nrDocumento='+this.cpf
        +'&nrTelefone='+this.telefone
        +'&imgCliente=null',
        {
          // key: Md5.init('EAS1234CHO5678'+this.cpf),
          // dsNomeCliente: this.nome,
          // dsEmail: this.email,
          // dtNascimento: this.data.split('-')[2] + '/' + this.data.split('-')[1] + '/' + this.data.split('-')[0],
          // dsSexo: this.sexo.feminino == true? 'F': this.sexo.masculino == true ? 'M':'-',
          // nrDocumento: this.cpf,
          // nrTelefone: this.telefone,
          // imgCliente: null
        }
      ).subscribe( (response) => {
        if (response.json().stIntegracao && response.json().stIntegracao == true){
          this._viewControl.dismiss({
            finalized: true
          })
        }else{
          this._alert.create({
            title: 'Erro ao cadastrar',
            subTitle: response.json().dsError
          }).present()
        }
      });
    }
  }

}
