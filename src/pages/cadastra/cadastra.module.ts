import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastraPage } from './cadastra';

@NgModule({
  declarations: [
    CadastraPage,
  ],
  imports: [
    IonicPageModule.forChild(CadastraPage),
  ],
})
export class CadastraPageModule {}
