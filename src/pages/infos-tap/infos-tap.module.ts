import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InfosTapPage } from './infos-tap';

@NgModule({
  declarations: [
    InfosTapPage,
  ],
  imports: [
    IonicPageModule.forChild(InfosTapPage),
  ],
})
export class InfosTapPageModule {}
