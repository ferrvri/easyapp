import { Component, OnInit } from '@angular/core';
import { NavController, IonicPage, ModalController, PopoverController } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { ExtratoPage } from '../extrato/extrato';
import { TapListPage } from '../tap-list/tap-list';
import { SobrePage } from '../sobre/sobre';
import { LoginPage } from '../login/login';
import { HomePopoverPage } from '../home-popover/home-popover';
import { SplashPage } from '../splash/splash';
import { Http } from '@angular/http';
import { ToastController } from 'ionic-angular';
import { RecargaModalPage } from '../recarga-modal/recarga-modal';

@IonicPage({name: 'HomePage'})
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{

  redes = [];
  userData = JSON.parse(localStorage.getItem('session')) || []

  constructor( private _toast:ToastController, private _http: Http, private _popover: PopoverController, private _modal: ModalController, public navCtrl: NavController, private sanitizer: DomSanitizer ) {

  }

  ngOnInit(): void {
    if (localStorage.getItem('session')){
      JSON.parse(localStorage.getItem('session')).Creditos.forEach(element => {
        this.redes.push(element)
      });
    }else{
      this.navCtrl.push(SplashPage)
    }
  }

  consultarExtrato(rede){
    this._modal.create(ExtratoPage, {idFilial: rede.idFilial, idCliente: rede.idEmpresa, creditos: rede.vlCreditoAtual}).present();
  }

  openTapList(rede){
    this._modal.create(TapListPage, rede).present();
  }

  openOptions(){
    let pop = this._popover.create(HomePopoverPage);
    let ev = {
      target : {
        getBoundingClientRect : () => {
          return {
            top: 50,
            left: window.innerWidth - 100
          };
        }
      }
    };
    
    pop.present({ev})
  }


  refreshSaldo(element: HTMLButtonElement, e){
    this._toast.create({
      message: 'Atualizando saldo...',
      showCloseButton: true,
      duration: 1200
    }).present()
    element.classList.toggle('turnAround');
    setTimeout( () => {
      if (element.classList.contains('turnAround')){
        element.classList.remove('turnAround');
      }
    }, 650); 
    this.getSaldo(e)
  }

  getSaldo(element){
    this._http.get(
      'https://apptst.easychopp.com.br/api/getCreditosCliente?key='+JSON.parse(localStorage.getItem('session')).hashKey+'&idEmpresa='+element.idEmpresa+'&idCliente='+JSON.parse(localStorage.getItem('session')).idCliente+'&idFilial='+element.idFilial,
      { 
      }
    ).subscribe( (response) => {
      if (response.json().Creditos && response.json().Creditos.length > 0){
        element.vlCreditoAtual = 0;
        setTimeout( () => {
          element.vlCreditoAtual = response.json().Creditos[0].vlCreditoAtual;
        }, 250);

        element.vlCreditoPendente = 0
        setTimeout( () => {
          element.vlCreditoPendente = response.json().Creditos[0].vlCreditoPendente;
        }, 250);
      }
    })
  }

  animImagem(element: HTMLDivElement, element2: HTMLSpanElement){
    element.classList.toggle('simpleBreath');
    element2.classList.toggle('simpleBreath');
    setTimeout( () => {
      if (element.classList.contains('simpleBreath')){
        element.classList.remove('simpleBreath')
      }

      if (element2.classList.contains('simpleBreath')){
        element2.classList.remove('simpleBreath')
      }
    }, 750);
  }

  recarregar(loja){
    let rec = this._modal.create(RecargaModalPage, {loja: loja});
    rec.present();
  }
}
