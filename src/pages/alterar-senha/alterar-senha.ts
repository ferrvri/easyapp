import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';

/**
 * Generated class for the AlterarSenhaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-alterar-senha',
  templateUrl: 'alterar-senha.html',
})
export class AlterarSenhaPage {

  senha_atual = '';
  new_senha = '';
  new_senha_conf = '';

  constructor( private _alert: AlertController, private _http: Http, public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AlterarSenhaPage');
  }


  alterarSenha(senha, nova, conf_nova){
    if (nova != conf_nova){
      this._alert.create({
        title: "Senhas não coincidem",
        message: "A nova senha digitada não coincide com a confirmação"
      }).present()
    }else if (senha.length < 1){

    }else if (nova.length < 8 || conf_nova.length < 8){
      this._alert.create({
        title: "Senha invalida!",
        message: "A nova senha deve possuir mais do que 8 caracteres."
      }).present();
    }else if (nova == '' || conf_nova == ''){
      this._alert.create({
        title: "Campos vazios",
        message: 'Preencha os campos para alterar a senha.'
      }).present();
    }else if (senha == ''){
      this._alert.create({
        title: "Campos vazios",
        message: 'Preencha os campos para alterar a senha.'
      }).present();
    }else{
      this._alert.create({
        title: 'Deseja realmente alterar?',
        buttons: [ {
          text: 'Não',
        },
        {
          text: 'Sim',
          handler: () => {
            let body = new FormData();
            body.append('idCliente', JSON.parse(localStorage.getItem('session')).idCliente);
            body.append('dsSenhaAtual', this.senha_atual)
            body.append('dsSenhaNova', this.new_senha_conf)

            this._http.post(
              'https://apptst.easychopp.com.br/api/alterarSenha',
              body
            ).subscribe( (response) => {
              if (!response.json().dsError){
                this._alert.create({
                  title: 'Alterada com sucesso!'
                }).present();
              }else{
                this._alert.create({
                  title: 'Erro ao alterar senha',
                  message: 'Erro ao alterar a senha, por favor, procure o suporte!'
                }).present();
              }
            });
          }
        }]
      }).present()
    }
  }
}
