import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecargaModalPage } from './recarga-modal';

@NgModule({
  declarations: [
    RecargaModalPage,
  ],
  imports: [
    IonicPageModule.forChild(RecargaModalPage),
  ],
})
export class RecargaModalPageModule {}
