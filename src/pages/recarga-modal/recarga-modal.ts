import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, ModalController, LoadingController } from 'ionic-angular';
import { Http, RequestOptions } from '@angular/http';
import { BrowserTab } from '@ionic-native/browser-tab';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { PagSeguroModalPage } from '../pag-seguro-modal/pag-seguro-modal';

@IonicPage()
@Component({
  selector: 'page-recarga-modal',
  templateUrl: 'recarga-modal.html',
})
export class RecargaModalPage implements OnInit {

  year = new Date().getFullYear();

  recarga_data = [
    {
      label: 'R$30,00 em Créditos',
      value: 30.00
    },
    {
      label: 'R$50,00 em Créditos',
      value: 50.00
    },
    {
      label: 'R$100,00 em Créditos',
      value: 100.00
    },
    {
      label: 'R$150,00 em Créditos',
      value: 150.00
    },
    {
      label: 'R$200,00 em Créditos',
      value: 200.00
    }
  ]

  constructor(private _load: LoadingController, private _modal: ModalController, private _httpCl: HttpClient, private _browser: BrowserTab, private _alert: AlertController, public viewCtrl: ViewController, private _http: Http, public navCtrl: NavController, public navParams: NavParams) {
  }

  ngOnInit(): void {
    // this.recarga_data = this.navParams.get('data');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecargaModalPage');
  }

  selecionar(opt) {
    let loading = this._load.create({
      showBackdrop: true,
      content: 'Aguardando solicitação...'
    });
    loading.present();

    let loja = this.navParams.get('loja');
    opt.value = parseFloat(opt.value).toFixed(2)

    let session = JSON.parse(localStorage.getItem('session'));
    let dtTransacao = new Date(Date.now());

    let _reference = session.idCliente + '|' + loja.idEmpresa + '|' + loja.seqCredito + '|' +
      new Date().getFullYear() + (new Date().getMonth() + 1 < 10 ? ('0' + (new Date().getMonth() + 1)) : (new Date().getMonth() + 1)) + new Date().getDate() + new Date().getHours()
      + new Date().getMinutes() + new Date().getSeconds();

    this._http.post(
      'http://technoxinformatica.com.br:21021/technox/tPagSeguro',
      // 'http://localhost:3000/technox/tPagSeguro',
      {
        email: loja.idUsuarioTransacaoOnLine,
        token: loja.SenhaTransacaoOnLine,
        currency: 'BRL',
        itemId1: '0001',
        itemDescription1: 'Credito EasyChopp',
        itemAmount1: opt.value,
        itemQuantity1: 1,
        itemWeight1: 1,
        reference: _reference,
        senderName: session.dsNomeCliente,
        senderEmail: localStorage.getItem('email'),
        shippingAddressRequired: false,
        timeout: 60,
        enableRecover: false
      }
    ).subscribe((response) => {
      if (response.text().indexOf('<code>') != -1 && response.text().indexOf('<error>') == -1) {
        let c = response.text().split('<code>');
        let cc = c[1].split('</code>')
        let cd = this._modal.create(PagSeguroModalPage, {
          codigo: cc[0]
        })

        cd.present();
        cd.onDidDismiss((data) => {
          let body = new FormData();
          body.append('Key', session.hashKey)
          body.append('idCliente', session.idCliente)
          body.append('idEmpresa', loja.idEmpresa)
          body.append('seqCredito', loja.seqCredito)
          body.append('idFilial', loja.idFilial)
          body.append('dtTransacao', dtTransacao.toISOString())
          body.append('vlValorTransacao', parseFloat(opt.value).toFixed(2))
          body.append('keyReferencia', _reference)
          body.append('tpOperadora', 'PAGSEGURO')
          body.append('stSituacao', '0')
          body.append('dtSituacao', dtTransacao.toISOString())

          this._http.post(
            'https://apptst.easychopp.com.br/api/addTransacaoOperadora',
            body
          ).subscribe((response) => {
            if (!response.json().dsError) {
              loading.dismiss();
            } else {
              loading.dismiss();
              let _a = this._alert.create({
                title: 'Erro!',
                message: 'Erro ao inserir transação no servidor EasyChopp, Informações recebidas do servidor: '+response.json().dsError+' -- code: 0x101f'
              });
              _a.present();
            }
          })
        });
      }else if (response.text().indexOf('<code>') != -1 && response.text().indexOf('<error>') != -1){
        let c = response.text().split('<message>');
        let cd = c[1].split('</message>');
        let _a = this._alert.create({
          title: 'Erro do PagSeguro',
          message: cd[0]
        });
        _a.present();
      } else {  
        if (response.json().status == false && response.json().msg && response.json().msg.length > 0){
          let _a = this._alert.create({
            title: 'Erro!',
            message: response.json().msg
          });
          _a.present();
        }else{
          let _a = this._alert.create({
            title: 'Erro!',
            message: 'Erro ao receber checkout, espere alguns minutos e tente novamente!, code: 0x103f'
          });
          _a.present();
        }
      }
      loading.dismiss();
    })
  }
}
