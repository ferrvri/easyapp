import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { Http } from '@angular/http';
import { TapViewerPage } from '../tap-viewer/tap-viewer';

/**
 * Generated class for the TapListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tap-list',
  templateUrl: 'tap-list.html',
})
export class TapListPage implements OnInit{

  isLoading = true;
  chopp_data = []
  
  constructor( private _modal: ModalController, private _http: Http, public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TapListPage');
  }

  ngOnInit(){
    this._http.get(
      'https://apptst.easychopp.com.br/api/getChopeiras?idFilial='+this.navParams.get('idFilial')+'&key='+JSON.parse(localStorage.getItem('session')).hashKey+'&idEmpresa='+this.navParams.get('idEmpresa')
    ).subscribe( (response) => {
      if (!response.json().dsError){
        if (response.json().Chopeiras && response.json().Chopeiras.length > 0){
          console.log(response.json())
          response.json().Chopeiras.forEach(element => {
            element.vlProduto = parseFloat(element.vlPrecoProduto100ml).toFixed(2)
            this.chopp_data.push(element)
          });
          this.isLoading = false
        }else{
          this.isLoading = false
        }
      }else{
        this.isLoading = false
      }
    }, () => {
      this.isLoading = false
    });
  }

  viewTap(tap){
    this._modal.create(TapViewerPage, tap).present()
  }

}
