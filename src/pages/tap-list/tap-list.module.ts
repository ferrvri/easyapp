import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TapListPage } from './tap-list';

@NgModule({
  declarations: [
    TapListPage,
  ],
  imports: [
    IonicPageModule.forChild(TapListPage),
  ],
})
export class TapListPageModule {}
