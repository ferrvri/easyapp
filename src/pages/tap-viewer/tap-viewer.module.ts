import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TapViewerPage } from './tap-viewer';

@NgModule({
  declarations: [
    TapViewerPage,
  ],
  imports: [
    IonicPageModule.forChild(TapViewerPage),
  ],
})
export class TapViewerPageModule {}
