import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, PopoverController } from 'ionic-angular';
import { Http } from '@angular/http';
import { DomSanitizer } from '@angular/platform-browser';
import { InfosTapPage } from '../infos-tap/infos-tap';

/**
 * Generated class for the TapViewerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tap-viewer',
  templateUrl: 'tap-viewer.html',
})
export class TapViewerPage implements OnInit{

  product_image:any;
  vlProduto;

  amargor = []
  corpo = []
  alcool = []

  constructor( private _popover: PopoverController, private sanitizer: DomSanitizer, private _http: Http, public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TapViewerPage');
  }

  ngOnInit(){
    console.log(this.navParams.data)
    this.vlProduto = parseFloat(this.navParams.get('vlPrecoProduto100ml')).toFixed(2);
    for (let i = 1; i <= 5; i++){
      if (i <= parseInt(this.navParams.get('vlCorpo'))){
        this.corpo.push(1)
      }else{
        this.corpo.push(0)
      }
    }

    for (let i = 1; i <= 5; i++){
      if (i <= parseInt(this.navParams.get('vlTeorAlcool'))){
        this.alcool.push(1)
      }else{
        this.alcool.push(0)
      }
    }

    for (let i = 1; i <= 5; i++){
      if (i <= parseInt(this.navParams.get('vlAmargor'))){
        this.amargor.push(1)
      }else{
        this.amargor.push(0)
      }
    }
  }

  viewInfos(data){
    this._popover.create(InfosTapPage, data).present();
  }

}
