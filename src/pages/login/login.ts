import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Http, RequestOptions, Headers } from '@angular/http';
import { SplashPage } from '../splash/splash';
import { CadastraPage } from '../cadastra/cadastra';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit{

  login: string = "";
  senha: string = "";

  showCadastroLabel = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _http: Http,
    private _alert: AlertController,
    private _modal: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    
  }

  entrar(u, s){
    if (u.length < 1 || s.length < 1){
      this._alert.create({
        title: 'Campos vazios!',
        subTitle: 'Preencha todos os campos para entrar!'
      }).present();
    }else{
      let body = new FormData();
      body.append('user', u); body.append('password', s)

      this._http.post(
        'https://apptst.easychopp.com.br/api/loginUser',
        body
      ).subscribe( (response: any) => {
        if (response.status == 200){
          if (response.json().Cliente && response.json().Cliente.dsNomeCliente !== undefined && response.json().Cliente.dsNomeCliente.length > 0){
            localStorage.setItem('session', JSON.stringify(response.json().Cliente))
            localStorage.setItem('email', u);
            this.navCtrl.push(SplashPage, {topath: 'HomePage', forroot: true});
          }else if (response.json().dsError.length > 0){
            this._alert.create({
              title: 'Erro!',
              message: response.json().dsError,
              buttons: ['OK']
            }).present();
          }
        }
      });
    }
  }

  openCadastro(){
    let m  = this._modal.create(CadastraPage)
    m.present()
    m.onDidDismiss( (data) => {
      if (data !== undefined && data.finalized && data.finalized == true){
        this.showCadastroLabel = true
      }
    });
  }
}
