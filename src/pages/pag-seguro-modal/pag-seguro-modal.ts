import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the PagSeguroModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pag-seguro-modal',
  templateUrl: 'pag-seguro-modal.html',
})
export class PagSeguroModalPage implements OnInit {

  url;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private sanitizer: DomSanitizer, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PagSeguroModalPage');
  }

  ngOnInit(){
    console.log('codigo', this.navParams.get('codigo'));
    // this.url = this.sanitizer.bypassSecurityTrustResourceUrl('https://pagseguro.uol.com.br/v2/checkout/payment.html?code='+this.navParams.get('codigo'));
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl('https://sandbox.pagseguro.uol.com.br/v2/checkout/payment.html?code='+this.navParams.get('codigo'));
  }
  
}
