import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PagSeguroModalPage } from './pag-seguro-modal';

@NgModule({
  declarations: [
    PagSeguroModalPage,
  ],
  imports: [
    IonicPageModule.forChild(PagSeguroModalPage),
  ],
})
export class PagSeguroModalPageModule {}
