import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { SobrePage } from '../sobre/sobre';
import { LoginPage } from '../login/login';
import { SplashPage } from '../splash/splash';
import { AlterarSenhaPage } from '../alterar-senha/alterar-senha';

/**
 * Generated class for the HomePopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home-popover',
  templateUrl: 'home-popover.html',
})
export class HomePopoverPage {

  userData = JSON.parse(localStorage.getItem('session')) || []

  constructor(public navCtrl: NavController, public navParams: NavParams, private _modal: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePopoverPage');
  }

  openSobre(){
    this._modal.create(SobrePage).present()
  }

  logOut(){
    localStorage.removeItem('session')
    this.navCtrl.push(SplashPage)
  }

  alterarSenha(){
    this._modal.create(AlterarSenhaPage).present();
  }
}
