import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { HomePage } from '../home/home';

declare var $: any;

@IonicPage()
@Component({
  selector: 'page-splash',
  templateUrl: 'splash.html',
})
export class SplashPage implements OnInit{

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    
  }

  ngOnInit(){
    $('.pour') //Pour Me Another Drink, Bartender!
      .delay(500)
      .animate({
        height: '160px'
      }, 1500)
      .delay(600)
      .slideUp(500);

    $('#liquid') // I Said Fill 'Er Up!
      .delay(1000)
      .animate({
        height: '120px'
      }, 2500);

    $('.beer-foam') // Keep that Foam Rollin' Toward the Top! Yahooo!
      .delay(1000)
      .animate({
        bottom: '110px'
      }, 2500);


      setTimeout( () => {
        if (this.navParams.get('topath')){
          if (this.navParams.get('forroot') == true){
            this.navCtrl.setRoot(this.navParams.get('topath'));
          }else{
            this.navCtrl.push(this.navParams.get('topath'));
          }
        }else{
          if (localStorage.getItem('session')){
            this.navCtrl.setRoot(HomePage);
          }else{
            this.navCtrl.setRoot(LoginPage);
          }
        }
      }, 3400);
  }

}
