import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { BrMaskerModule } from 'brmasker-ionic-3';

import { MyApp } from './app.component';
import { SplashPage } from '../pages/splash/splash';
import { LoginPage } from '../pages/login/login';
import { HttpModule } from '@angular/http';
import { HomePageModule } from '../pages/home/home.module';
import { ExtratoPage } from '../pages/extrato/extrato';
import { TapListPage } from '../pages/tap-list/tap-list';
import { TapViewerPage } from '../pages/tap-viewer/tap-viewer';
import { SobrePage } from '../pages/sobre/sobre';
import { InfosTapPage } from '../pages/infos-tap/infos-tap';
import { HomePopoverPage } from '../pages/home-popover/home-popover';
import { CadastraPage } from '../pages/cadastra/cadastra';
import { RecargaModalPage } from '../pages/recarga-modal/recarga-modal';
import { BrowserTab } from '@ionic-native/browser-tab';
import { HttpClientModule } from '@angular/common/http';
import { PagSeguroModalPage } from '../pages/pag-seguro-modal/pag-seguro-modal';
import { AlterarSenhaPage } from '../pages/alterar-senha/alterar-senha';

@NgModule({
  declarations: [
    MyApp,
    SplashPage,
    LoginPage,
    ExtratoPage,
    TapListPage,
    TapViewerPage,
    SobrePage,
    InfosTapPage,
    HomePopoverPage,
    CadastraPage,
    RecargaModalPage,
    PagSeguroModalPage,
    AlterarSenhaPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false
    }),
    HttpModule,
    HomePageModule,
    BrMaskerModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SplashPage,
    LoginPage,
    ExtratoPage,
    TapListPage,
    TapViewerPage,
    SobrePage,
    InfosTapPage,
    HomePopoverPage,
    CadastraPage,
    RecargaModalPage,
    PagSeguroModalPage,
    AlterarSenhaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    BrowserTab
  ]
})
export class AppModule { }
