const express = require('express');
const corst = require('cors')
const app = express();
const bodyParser = require('body-parser')
const request = require('request')

app.use(corst());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.post('/technox/tPagSeguro', (req, res) => {
    if (req.body.email && req.body.token){
        let c = `https://ws.pagseguro.uol.com.br/v2/checkout?email=${req.body.email}&token=${req.body.token}&currency=${req.body.currency}&itemId1=${req.body.itemId1}&itemDescription1=${req.body.itemDescription1}&itemAmount1=${req.body.itemAmount1}&itemQuantity1=${req.body.itemQuantity1}&itemWeight1=${req.body.itemWeight1}&reference=${req.body.reference}&senderName=${req.body.senderName}&senderEmail=${req.body.senderEmail}&shippingAddressRequired=False&timeout=${req.body.timeout}&enableRecover=${req.body.enableRecover}&installmentQuantity=1`;
        
        request({
            url: c,
            method: "POST",
            headers: {
                "content-type": "application/x-www-form-urlencoded",  // <--Very important!!!
            },
            body: ''
        }, function (error, response, body){
            console.log(' error ', error);
            res.end(response.body);
        });

        res.end()
    }
}).listen(21021);

console.log('Started');